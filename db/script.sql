CREATE TABLE IF NOT EXISTS ticketing (
    id INT(11) NOT NULL AUTO_INCREMENT,
    ip_caller VARCHAR(50),
    ip_server VARCHAR(50),
    url_path_server VARCHAR(255) ,
    hospital VARCHAR(150) ,
    department VARCHAR(100) ,
    patient VARCHAR(100) ,
    username VARCHAR(100) ,
    description VARCHAR(4000),
    attachment MEDIUMBLOB,
    PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;  


commit;