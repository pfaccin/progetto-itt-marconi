# Progetto FE

## Creazione Applicazione Angular
Creare una nuova applicazione Angular con il seguente comando:

```console
ng new gpi-tickets
```

## Workspace di Visual Studio Code
Seguire la seguente procedura per modificare il codice dell'applicazione
Angular con Visual Studio Code:

- copiare il contenuto della cartella **vscode workspace** all'interno della
cartella **gpi-tickets-app** generata tramite Angular CLI
- in Visual Studio Code, aprire il file **workspace.code-workspace** cliccando
su *File -> Open Workspace...* oppure facendo doppio click sul file
- Installare tutte le estensioni suggerite dal workspace

Da linea di comando, all'interno della cartella **gpi-tickets-app**, eseguire
il seguente comando per installare alcune dipendenze richieste dal workspace:

```console
npm i -D htmlhint prettier prettier-stylelint stylelint stylelint-config-standard tslint-angular tslint-config-prettier
```

## Progetto
Il progetto prevede di implementare una pagina in Angular simile a quella
raggiungibile a questo indirizzo: [http://itt-marconi-rovereto-2019.s3-website.eu-central-1.amazonaws.com](http://itt-marconi-rovereto-2019.s3-website.eu-central-1.amazonaws.com)
